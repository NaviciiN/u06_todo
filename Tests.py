import unittest
from unittest.mock import patch, mock_open
import json
from todo import load_tasks, add_task, complete_task, list_tasks

class TodoListTests(unittest.TestCase):
    def test_load_tasks_file_found(self):
        # Simulate a tasks.json file with some data
        tasks_data = '[{"name": "Task 1", "completed": false}, {"name": "Task 2", "completed": true}]'
        with patch('builtins.open', mock_open(read_data=tasks_data)):
            tasks = load_tasks()
        self.assertEqual(len(tasks), 2)
        self.assertEqual(tasks[0]["name"], "Task 1")
        self.assertFalse(tasks[0]["completed"])
        self.assertEqual(tasks[1]["name"], "Task 2")
        self.assertTrue(tasks[1]["completed"])

    def test_load_tasks_file_not_found(self):
        with patch('builtins.open', side_effect=FileNotFoundError):
            tasks = load_tasks()
        self.assertEqual(tasks, [])

    def test_add_task(self):
        tasks = []
        with patch('builtins.input', return_value="New Task"):
            add_task(tasks)
        self.assertEqual(len(tasks), 1)
        self.assertEqual(tasks[0]["name"], "New Task")
        self.assertFalse(tasks[0]["completed"])

    def test_complete_task_valid_index(self):
        tasks = [{"name": "Task 1", "completed": False}]
        with patch('builtins.input', return_value="0"):
            complete_task(tasks)
        self.assertTrue(tasks[0]["completed"])

    def test_complete_task_invalid_index(self):
        tasks = [{"name": "Task 1", "completed": False}]
        with patch('builtins.input', return_value="1"):
            complete_task(tasks)
        self.assertFalse(tasks[0]["completed"])

    def test_list_tasks(self):
        tasks = [
            {"name": "Task 1", "completed": False},
            {"name": "Task 2", "completed": True}
        ]
        expected_output = "Tasks:\n0. [ ] Task 1\n1. [X] Task 2\n"
        with patch('builtins.print') as mock_print:
            list_tasks(tasks)
        mock_print.assert_called_with(expected_output)

if __name__ == '__main__':
    unittest.main()
